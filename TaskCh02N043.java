package com.company;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int temp1 = a % b;
        int temp2 = b % a;
        System.out.print(temp1 * temp2 + 1);
    }
}
