package com.company;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if ((n >= 100) && (n <= 999)) {
            int nFirst = n / 100;
            int nSecond = (n / 10) % 10;
            int nThird = n % 10;
            int x = nFirst* 100 + (nThird * 10) + nSecond;
            System.out.println("n: " + n);
            System.out.print("x: "+ x);
        } else {
            System.out.print("Введено число вне диапазона 100 <= n <= 999");
        }
    }
}
