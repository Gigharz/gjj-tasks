package com.company;

import java.util.Scanner;

public class TaskCh01N017 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double angle = in.nextDouble();
        double x = in.nextDouble();
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();

        double sin = Math.sin(angle);
        double resultO = Math.pow(1 - Math.pow(sin, 2), 0.5);
        System.out.println(resultO);

        double resultP = 1 / Math.pow(a * Math.pow(x, 2) + b * x + c, 0.5);
        System.out.println(resultP);

        double resultR = Math.pow(x + 1, 0.5) + Math.pow(x - 1, 0.5) / 2 * Math.pow(x, 0.5);
        System.out.println(resultR);

        double resultS = Math.abs(x) + Math.abs(x + 1);
        System.out.println(resultS);

    }
}
