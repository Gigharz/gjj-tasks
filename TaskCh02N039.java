package com.company;

import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int h = in.nextInt();
        int m = in.nextInt();
        int s = in.nextInt();

        boolean checkInputH = h >=0 & h <= 23;
        boolean checkInputM = h >=0 & h <= 59;
        boolean checkInputS = h >=0 & h <= 59;

        if (checkInputH && checkInputM && checkInputS) {
            int halfDay = h % 12;
            int sec = halfDay * 3600 + m + s;
            double result = 360 * sec / 12 / 3600;
            System.out.print("Angle = " + result);
        } else {
            System.out.print("Wrong input");
        }
    }
}
