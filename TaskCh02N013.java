package com.company;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if ((n > 100) && (n < 200)) {
            int nFirst = n / 100;
            int nSecond = (n / 10) % 10;
            int nThird = n % 10;
            int result = nFirst + nSecond + nThird;
            System.out.print(result);
        } else {
            System.out.print("Введено число вне диапазона 100 < n < 200");
        }
    }
}
