package com.company;

import java.util.Scanner;

public class TaskCh03N029 {
    public static void main(String[] args) {
        boolean a;
        int x, y, z;
        Scanner scanner = new Scanner(System.in);
        x = scanner.nextInt();
        y = scanner.nextInt();
        z = scanner.nextInt();
        /**
        * Вариант а
         */
        if (x % 2 == 0) {
            if (y % 2 == 0) {
                a = true;
            } else {
                a = false;
            }
        } else {
            a = false;
        }
        System.out.println("a) " + a);

        /**
         * Вариант б
         */

        boolean t1 = x < 20;
        boolean t2 = y < 20;
        a = t1 ^ t2;
        System.out.println("b) " + a);

        /**
         * Вариант в
         */
        if (x == 0) {
            a = true;
        } else if (y == 0) {
            a = true;
        } else {
            a = false;
        }
        System.out.println("v) " + a);
        /**
         * Вариант г
         */
        if (x < 0) {
            if (y < 0) {
                if (z < 0) {
                    a = true;
                } else {
                    a = false;
                }
            }
        }
        System.out.println("g) " + a);
        /**
         * Вариант д
         */
        boolean x1 = x % 5 == 0, b1 = y % 5 == 0, c1 = z % 5 == 0;
        a = !( x1 && b1 && c1 ) && ( x1 ^ b1 ^ c1 );
        System.out.println("d) " + a);
        /**
         * Вариант е
         */
        if (x > 100) {
            a = true;
        } else if (y > 100) {
            a = true;
        } else if (z > 100) {
            a = true;
        } else {
            a = false;
        }
        System.out.println("e) " + a);
    }
}
